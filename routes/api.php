<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ArticleTag;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['role:User|Admin|Writer']], function () {
    Route::get('/user/{id}', [UserController::class, 'show'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::post('/comment', [CommentController::class, 'store'])->middleware('auth:api');
});

Route::group(['middleware' => ['role:Admin|Writer']], function () {
    Route::delete('/tag/{id}', [TagController::class, 'destroy'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::get('/tag/{id}', [TagController::class, 'show'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::post('/tag', [TagController::class, 'store'])->middleware('auth:api');
    Route::post('/article', [ArticleController::class, 'store'])->middleware('auth:api');
    Route::get('/article-tag', [ArticleTag::class, 'index'])->middleware('auth:api');
    Route::post('/article-tag', [ArticleTag::class, 'store'])->middleware('auth:api');
});

Route::group(['middleware' => ['role:Admin']], function () {
    Route::get('/users', [UserController::class, 'index'])->middleware('auth:api');
    Route::put('/user/change-role/{id}', [UserController::class, 'changeRole'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::get('/users/delete', [UserController::class, 'getDeleted'])->middleware('auth:api');
    Route::put('/user/restore/{id}', [UserController::class, 'restore'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::delete('/user/{id}', [UserController::class, 'destroy'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::delete('/article/{id}', [ArticleController::class, 'destroy'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::delete('/article/restore/{id}', [ArticleController::class, 'restore'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::put('/comment/restore/{id}', [CommentController::class, 'restore'])->where('id', '[0-9]+')->middleware('auth:api');
    Route::delete('/comment/{id}', [CommentController::class, 'destroy'])->where('id', '[0-9]+')->middleware('auth:api');
});

Route::post('/user', [UserController::class, 'store']);
Route::post('/user/login', [UserController::class, 'login']);
Route::post('/user/logout', [UserController::class, 'logout']);
Route::get('/tags', [TagController::class, 'index']);
Route::get('/articles', [TagController::class, 'index']);
Route::get('/article/{id}', [ArticleController::class, 'show']);



