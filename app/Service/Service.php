<?php

namespace App\Service;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class Service
{
    /**
     * @param string $model Model name
     * @param $skip ?int skip
     * @param $limit ?int max limit
     * @return AnonymousResourceCollection
     */
    public function getAll(string $model, ?int $skip = 0, ?int $limit = 10, bool $showDeleted = false): AnonymousResourceCollection
    {
        if ($showDeleted) {
            return $model::dtoCollection($model::onlyTrashed()->get()->skip($skip)->take($limit));
        }

        return $model::dtoCollection($model::all()->skip($skip)->take($limit));
    }

    /**
     * @param string $model Model name
     * @param int $id $id
     * @return mixed
     */
    public function getOne(string $model, int $id): mixed
    {
        $data = $model::findOrFail($id);
        return $model::dto($data);
    }

    /**
     * @param string $model
     * @param array $data
     * @return mixed
     */
    public function save(string $model, array $data): mixed
    {
       $newRecord = $model::create($data);
       return $model::dto($newRecord);
    }

    /**
     * @param string $model Model Name
     * @param int $id id
     * @return void
     */
    public function softDelete(string $model, int $id): void
    {
        $find = $model::findOrFail($id);
        $find->delete();
    }

    /**
     * @param string $model Model Name
     * @param int $id id
     * @return mixed
     */
    public function restoreDeleteItem(string $model, int $id): mixed
    {
        $find = $model::onlyTrashed()->findOrFail($id);
        $find->restore();
        return $model::dto($find);
    }
}
