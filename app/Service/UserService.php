<?php

namespace App\Service;

use App\Dto\UserDto;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserService extends Service
{
    /**
     * @param array $data login|password
     * @param Request $request http request
     * @return array|JsonResponse
     */
    public function login(array $data, Request $request): array|JsonResponse
    {
        if (!Auth::attempt($data)) {
            return response()->json("Invalid credentials", 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        return [
            'user' => UserDto::make(Auth::user()),
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateString(),
        ];
    }

    /**
     * @param Request $request http request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()->revoke();
        return response()->json("The user has been logged out", 200);
    }

    public function changeRole(int $userId, int $roleId)
    {
        $user = User::findOrFail($userId);
        $user->removeRole($user->role_id);
        $user->assignRole($roleId);

        return UserDto::make($user);
    }
}
