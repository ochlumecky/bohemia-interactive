<?php

namespace App\Http\Controllers;

use App\Dto\ArticleDto;
use App\Models\Article;
use App\Models\Tag;
use App\Service\ArticleService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Nette\Schema\Expect;
use Nette\Schema\Processor;

class ArticleController extends Controller
{
    private $service;

    public function __construct(Processor $processor, ArticleService $service)
    {
        $this->service = $service;
        parent::__construct($processor);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return $this->service->getAll(Article::class, $request->get('offset'), $request->get('limit'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return ArticleDto
     */
    public function store(Request $request): ArticleDto
    {
        $schema = Expect::structure([
            'user_id' => Expect::int()->required(),
            'text' => Expect::string()->max(64)->required(),
            'title' => Expect::string()->max(20)->required(),
        ]);

        $normalized = $this->processor->process($schema, $request->all());

        return $this->service->save(Article::class, (array)$normalized);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return ArticleDto
     */
    public function show(int $id): ArticleDto
    {
        return $this->service->getOne(Article::class, $id);
    }

    /**
     * @param int $id
     * @return ArticleDto
     */
    public function restore(int $id): ArticleDto
    {
        return $this->service->restoreDeleteItem(Article::class, $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy(int $id): void
    {
        $this->service->softDelete(Article::class, $id);
    }
}
