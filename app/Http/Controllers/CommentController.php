<?php

namespace App\Http\Controllers;

use App\Dto\CommentDto;
use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use App\Service\CommentService;
use App\Service\Service;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Nette\Schema\Expect;
use Nette\Schema\Processor;

class CommentController extends Controller
{
    private Service $service;

    public function __construct(Processor $processor, CommentService $service)
    {
        $this->service = $service;
        parent::__construct($processor);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return CommentDto
     */
    public function store(Request $request): CommentDto
    {
        $schema = Expect::structure([
            'user_id' => Expect::int()->min(1)->required(),
            'article_id' => Expect::int()->min(1)->required(),
            'text' => Expect::string()->min(1)->max(255)->required(),
        ]);

        $normalized = $this->processor->process($schema, $request->all());

        return $this->service->save(Comment::class, (array)$normalized);
    }

    /**
     * @param int $id
     * @return CommentDto
     */
    public function restore(int $id): CommentDto
    {
        return $this->service->restoreDeleteItem(Comment::class, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy(int $id): void
    {
        $this->service->softDelete(Comment::class, $id);
    }
}
