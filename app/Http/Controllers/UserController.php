<?php

namespace App\Http\Controllers;

use App\DataTransferObjects\ResponseData;
use App\Dto\User\UserDto;
use App\Http\Helper;
use App\Models\User;
use App\Service\UserService;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Nette\Schema\Expect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Nette\Schema\Processor;

class UserController extends Controller
{
    private $service;

    public function __construct(Processor $processor, UserService $service)
    {
        $this->service = $service;
        parent::__construct($processor);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return $this->service->getAll(User::class, $request->get('offset'), $request->get('limit'));
    }

    /**
     * Get all deleted users
     *
     * @return AnonymousResourceCollection
     */
    public function getDeleted(Request $request): AnonymousResourceCollection
    {
        return $this->service->getAll(User::class, $request->get('offset'), $request->get('limit'), true);
    }

    /**
     * Get all deleted users
     *
     * @return \App\Dto\UserDto
     */
    public function restore(int $id): \App\Dto\UserDto
    {
        return $this->service->restoreDeleteItem(User::class, $id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $schmema = Expect::structure([
            "nickname" => Expect::string()->min(1)->required(),
            "password" => Expect::string()->min(1)->required()
        ]);

        $normalized = $this->processor->process($schmema, $request->all());

        return response()->json(['data' => [
            $this->service->login((array)$normalized, $request)
        ]]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        return $this->service->logout($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \App\Dto\UserDto
     */
    public function store(Request $request): \App\Dto\UserDto
    {
        $schema = Expect::structure([
            'name' => Expect::string()->min(3)->required(),
            'surname' => Expect::string()->min(5)->required(),
            'password' => Expect::string()->min(10)->required(),
            'phone' => Expect::string()->min(8)->required(),
            'email' => Expect::email()->required(),
            'address' => Expect::string()->min(1)->required(),
            'city' => Expect::string()->min(1)->required(),
            'state' => Expect::string()->min(1)->required(),
            'zip' => Expect::string()->min(1)->required(),
        ]);

        $normalized = $this->processor->process($schema, $request->all());
        $normalized->created_at = new DateTime();
        $normalized->updated_at = new DateTime();
        $normalized->nickname = Helper::removeDiacritics(strtolower($normalized->surname . substr($normalized->name, 0, 3)));
        $normalized->password = Hash::make($normalized->password);
        $normalized->role_id = 3;

        return $this->service->save(User::class, (array)$normalized);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \App\Dto\UserDto
     */
    public function show(int $id): \App\Dto\UserDto
    {
        return $this->service->getOne(User::class, $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy(int $id): void
    {
        $this->service->softDelete(User::class, $id);
    }

    /**
     * @param int $id
     * @return \App\Dto\UserDto
     */
    public function changeRole(Request $request, int $id): \App\Dto\UserDto
    {
        $schema = Expect::structure([
            'role_id' => Expect::int()->min(1)->max(3)->required(),
        ]);
        $normalized = $this->processor->process($schema, $request->all());
        return $this->service->changeRole($id, (int)$normalized);
    }
}
