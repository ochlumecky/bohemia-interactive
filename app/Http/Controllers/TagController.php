<?php

namespace App\Http\Controllers;

use App\Dto\TagDto;
use App\Models\Tag;
use App\Service\TagService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Nette\Schema\Expect;
use Nette\Schema\Processor;

class TagController extends Controller
{
    private $service;

    public function __construct(Processor $processor, TagService $service)
    {
        $this->service = $service;
        parent::__construct($processor);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
       return $this->service->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request): Response
    {
        $schema = Expect::structure([
            'name' => Expect::string()->min(3)->max(6)->required()
        ]);

        $normalized = $this->processor->process($schema, $request->all());

        return $this->service->save(Tag::class, (array)$normalized);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return TagDto
     */
    public function show(int $id): TagDto
    {
        return $this->service->getOne(Tag::class, $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return void
     */
    public function destroy(int $id): void
    {
        $this->service->softDelete(Tag::class, $id);
    }
}
