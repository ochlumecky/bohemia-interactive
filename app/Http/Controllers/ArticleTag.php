<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleTags;
use App\Models\Tag;
use App\Service\ArticleTagService;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Date;
use Nette\Schema\Expect;
use Nette\Schema\Processor;

class ArticleTag extends Controller
{
    private $service;

    public function __construct(Processor $processor, ArticleTagService $service)
    {
        $this->service = $service;
        parent::__construct($processor);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
       return $this->service->getAll(ArticleTags::class, $request->get('offset'), $request->get('limit'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schema = Expect::structure([
            'article_id' => Expect::int()->min(1)->max(Article::count()+1)->required(),
            'tag_id' => Expect::int()->min(1)->max(Tag::count()+1)->required(),
        ]);

        $normalized = $this->processor->process($schema, $request->all());

        return $this->service->save(ArticleTags::class, (array)$normalized);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->getOne(ArticleTags::class, $id);
    }
}
