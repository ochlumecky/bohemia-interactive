<?php

namespace App\Observers;

use App\Models\User;

class UserObserver
{
    public function created(User $user): void
    {
        if ($user->wasRecentlyCreated) {
            $user->assignRole($user->role);
        }
    }

    public function deleted(User $user): void
    {
        $user->removeRole($user->role_id);
    }

    public function restored(User $user): void
    {
        $user->assignRole($user->role);
    }
}
