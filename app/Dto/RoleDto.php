<?php

namespace App\Dto;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleDto extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
        ];
    }
}
