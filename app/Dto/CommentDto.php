<?php

namespace App\Dto;

use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentDto extends JsonResource
{
    public function toArray($request)
    {
        return [
            'user' => UserDto::make($this->user),
            'text' => $this->text,
            'created' => (new DateTime($this->created_at))->format("d.m.Y H:m:s"),
        ];
    }
}
