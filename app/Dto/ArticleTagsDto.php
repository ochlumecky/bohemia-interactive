<?php

namespace App\Dto;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleTagsDto extends JsonResource
{
    public function toArray($request)
    {
        return TagDto::make($this->tag);
    }
}
