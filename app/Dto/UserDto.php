<?php

namespace App\Dto;

use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDto extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'nickname' => $this->nickname,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'zip' => $this->zip,
            'role' => RoleDto::make($this->role),
            'created_at' => (new DateTime($this->created_at))->format('d.m.Y H:m:s'),
            'deleted' => $this->trashed(),
            'delete_at' => $this->trashed() ? (new DateTime($this->deleted_at))->format('d.m.Y H:m:s') : null,
        ];
    }
}
