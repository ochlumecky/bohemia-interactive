<?php

namespace App\Dto;

use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleDto extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => UserDto::make($this->user),
            "title" => $this->title,
            "text" => $this->text,
            "created_at" => (new DateTime($this->created_at))->format('d.m.Y H:m:s'),
            "tags" => ArticleTagsDto::collection($this->tags),
            "comment" => CommentDto::collection($this->comment)
        ];
    }
}
