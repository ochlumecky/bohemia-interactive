<?php

namespace App\Models;

use App\Dto\UserDto;
use App\Events\UserSaved;
use DateTime;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $nickname
 * @property string $password
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $city
 * @property string $state
 * @property int $zip
 * @property int $role_id
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property DateTime $deteled_at
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes, HasApiTokens;

    protected $fillable = [
        'name',
        'surname',
        'nickname',
        'password',
        'phone',
        'email',
        'address',
        'city',
        'state',
        'zip',
        'role_id',
        'created_at',
        'update_at',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public static function dto($data)
    {
        return UserDto::make($data);
    }

    public static function dtoCollection(Collection $data): AnonymousResourceCollection
    {
        return UserDto::collection($data);
    }
}


