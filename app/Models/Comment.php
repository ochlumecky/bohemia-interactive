<?php

namespace App\Models;

use App\Dto\ArticleDto;
use App\Dto\CommentDto;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @property int $user_id
 * @property int $article_id
 * @property string $text
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Comment extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'article_id',
        'text',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function dto($data)
    {
        return CommentDto::make($data);
    }

    public static function dtoCollection(Collection $data): AnonymousResourceCollection
    {
        return CommentDto::collection($data);
    }
}
