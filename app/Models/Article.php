<?php

namespace App\Models;

use App\Dto\ArticleDto;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @property int $id
 * @property int $user_id
 * @property string $text
 * @property string$title
 */
class Article extends Model
{
    use HasFactory; use SoftDeletes;

    protected $fillable = [
        'user_id',
        'text',
        'title',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->hasMany(ArticleTags::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public static function dto($data)
    {
        return ArticleDto::make($data);
    }

    public static function dtoCollection(Collection $data): AnonymousResourceCollection
    {
        return ArticleDto::collection($data);
    }
}
