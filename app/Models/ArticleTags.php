<?php

namespace App\Models;

use App\Dto\ArticleTagsDto;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ArticleTags extends Model
{
    use HasFactory; use SoftDeletes;

    protected $fillable = [
        'article_id',
        'tag_id',
        'created_at',
        'updated_at',
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public static function dto($data)
    {
        return ArticleTagsDto::make($data);
    }

    public static function dtoCollection(Collection $data): AnonymousResourceCollection
    {
        return ArticleTagsDto::collection($data);
    }
}
