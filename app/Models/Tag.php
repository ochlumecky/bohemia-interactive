<?php

namespace App\Models;

use App\Dto\TagDto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Tag extends Model
{
    use HasFactory; use SoftDeletes;

    protected $fillable = [
        'id',
        'name',
    ];

    public static function dto($data)
    {
        return TagDto::make($data);
    }

    public static function dtoCollection(Collection $data): AnonymousResourceCollection
    {
        return TagDto::collection($data);
    }
}
