<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\ArticleTags;
use App\Models\Comment;
use Spatie\Permission\Models\Role;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    private $userPermissions = [
        'user_index',
        'user_getDeleted',
        'user_show',
        'user_restore',
        'user_store',
        'user_destroy',
    ];
    private $tagPermissions = [
        'tag_index',
        'tag_show',
        'tag_store',
        'tag_destroy',
    ];
    private $articlePermissions = [
        'article_index',
        'article_store',
    ];

    private $Admin = [];

    private $Writer = [
        'tag_index',
        'tag_show',
        'tag_store',
        'article_store',
    ];
    private $User = [
        'user_index',
        'user_show',
        'article_index',
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(5)->create();
        Tag::factory(10)->create();
        Article::factory(10)->create();
        Comment::factory(20)->create();
        ArticleTags::factory(30)->create();


        $permissions = array_merge($this->userPermissions, $this->tagPermissions, $this->articlePermissions);
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }


        foreach (Role::all() as $role) {
            $permissions = $this->{$role->name};

            if ($role->name === 'Admin') {
                $permissions = array_merge($this->userPermissions, $this->tagPermissions, $this->articlePermissions);
            }

            $this->givePermission($role, $permissions);
        }

        $users = User::all();
        foreach ($users as $user) {
            $user->assignRole($user->role);
        }
    }

    private function givePermission(Role $role, array $permissions): void
    {
        foreach ($permissions as $permission) {
            $per = Permission::findByName($permission);
            $role->givePermissionTo($per);
            $per->assignRole($role);
        }
    }
}
